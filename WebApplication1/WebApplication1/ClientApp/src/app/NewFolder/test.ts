import { Component } from '@angular/core';

@Component({
  selector: 'app-test-component',
  templateUrl: './test.html'
})
export class TestComponent {
  public currentCount = 0;

  public incrementCounter() {
    this.currentCount++;
  }
}
