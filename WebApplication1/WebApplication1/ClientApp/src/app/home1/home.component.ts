import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class MyHomeComponent implements OnInit {
  constructor(private data: DataService) { }

  users: Object;

  h1Style: boolean = false;

  firstClick() {
    this.h1Style = !this.h1Style;
    this.data.firstClick();
    //console.log("clicked");
  }

  ngOnInit() {
    this.data.getUsers().subscribe(data => {
      this.users = data;
      console.log(data);
    })

  }
}
